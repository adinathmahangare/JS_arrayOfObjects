//    Write a function that accesses and logs the name and city of the individual at the index position 3 in the dataset.

function getNameCityIndex3(dataSet) {
    
    if(typeof dataSet == "object" && dataSet!== null && typeof dataSet.length=='number') {
        for (const person of dataSet) {
            if (person.id === 3) {
                console.log('Name: '+ person.name + ', City: '+ person.city);
            }
        }
    } else {
        return ;
    }
}

module.exports = getNameCityIndex3;