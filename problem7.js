//    Write a function that accesses and prints the names and email addresses of individuals aged 25.

function getNameEmailAge25(dataSet) {
    
    if(typeof dataSet == "object" && dataSet!== null && typeof dataSet.length=='number') {
        for (const person of dataSet) {
            if (person.age === 25) {
                console.log('Name: ' + person.name + ', Email: ' + person.email);
            }
        }
    } else {
        return ;
    }
}

module.exports = getNameEmailAge25;