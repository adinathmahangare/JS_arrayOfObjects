//    Create a function to retrieve and display the first hobby of each individual in the dataset.

function getFirstHobby(dataSet) {
    
    if(typeof dataSet == "object" && dataSet!== null && typeof dataSet.length=='number') {
        for (const person of dataSet) {
            console.log(person.hobbies[0]);
        }
    } else {
        return ;
    }
}

module.exports = getFirstHobby;