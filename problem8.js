 //    Implement a loop to access and log the city and country of each individual in the dataset.

function getAllCityCountry(dataSet){
    
    if(typeof dataSet == "object" && dataSet!== null && typeof dataSet.length=='number') {
        for (const person of dataSet) {
            console.log('City: ' + person.city + ', Country: ' + person.country);
        }
    } else {
        return ;
    }
}

module.exports = getAllCityCountry;