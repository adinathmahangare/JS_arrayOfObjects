//    Implement a loop to access and print the ages of all individuals in the dataset.

function getAllAges(dataSet) {
    
    if(typeof dataSet == "object" && dataSet!== null && typeof dataSet.length=='number') {
        for (const person of dataSet) {
            console.log(person.age);
        }
    } else {
        return ;
    }
}

module.exports = getAllAges;