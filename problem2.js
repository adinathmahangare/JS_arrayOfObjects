//    Implement a function that retrieves and prints the hobbies of individuals with a specific age, say 30 years old.

function getHobbiesWithAge30(dataSet){
    if(typeof dataSet == "object" && dataSet!== null && typeof dataSet.length=='number') {
        for (const person of dataSet){
            if (person.age < 30){
                console.log(person.hobbies)            
            }
        }
    } else {
        return ;
    }
}

module.exports = getHobbiesWithAge30;