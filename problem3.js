//    Create a function that extracts and displays the names of individuals who are students (`isStudent: true`) and live in Australia.

function getStudentAustralia(dataSet) {
    
    if(typeof dataSet == "object" && dataSet!== null && typeof dataSet.length=='number') {
        for (const person of dataSet) {
            if (person.isStudent === true && person.country === 'Australia'){
                console.log(person.name);
            }
        }
    } else {
        return ;
    }
}

module.exports = getStudentAustralia;